const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors('*'))

app.use(express.json())

const router = require('./routes/movies')
app.use('/movies', router)

app.listen(3000, () => {
    console.log("user server started successfully on port 3000")
})