const mysql = require('mysql2')

const pool = mysql.createPool({
    host: '172.17.0.3',
    database: 'exam',
    user: 'root',
    password: 'manager',
    waitForConnections: true,
})

module.exports = { pool, }