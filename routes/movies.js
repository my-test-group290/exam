const { response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

//Movie_Tb --> (movie_id, movie_title, movie_release_date,movie_time,director_name)  


router.get('/', (request, response) => {
    const name = request.query.name
    const query = 'select movie_title, movie_release_date, movie_time, director_name from Movie_Tb where movie_title = ?'
    db.pool.execute(query, [name], (error, user) => {
        response.send(utils.createResult(error, user))
    })
})

router.post('/', (request, response) => {
    const body = request.body
    const query = 'insert into Movie_Tb values (?,?,?,?,?)'
    db.pool.execute(query, [body.movie_id, body.movie_title, body.movie_release_date, body.movie_time, body.director_name], (error, user) => {
        response.send(utils.createResult(error, user))
    })
})

router.delete('/', (request, response) => {
    const name = request.query.name
    const query = 'delete from Movie_Tb where movie_title = ?'
    db.pool.execute(query, [name], (error, user) => {
        response.send(utils.createResult(error, user))
    })
})

module.exports = router